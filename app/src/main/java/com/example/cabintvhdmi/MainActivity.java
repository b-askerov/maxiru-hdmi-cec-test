package com.example.cabintvhdmi;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.hardware.hdmi.HdmiDeviceInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.content.Context;
import android.hardware.hdmi.HdmiControlManager;
import android.hardware.hdmi.HdmiPlaybackClient;

import java.lang.reflect.Field;
import java.util.Hashtable;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Context context = this;

        final TextView textResult = findViewById(R.id.textResult);
        final Button button = findViewById(R.id.mainButton);
        final Button pwrButton = findViewById(R.id.mainPwrButton);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                button.setClickable(false);
                button.setVisibility(View.INVISIBLE);

                final StringBuilder resultOutput = new StringBuilder();
                resultOutput.append("Test result:\n");

                try {
                    @SuppressLint("WrongConstant")
                    final HdmiControlManager hdmiManager = (HdmiControlManager) context.getSystemService(Context.HDMI_CONTROL_SERVICE);
                    resultOutput.append("HdmiControlManager: Connected\n");

                    if (hdmiManager == null) {
                        throw new Exception("hdmiControlManager is null");
                    }

                    // not working, why? "No virtual method setStandbyMode"
                    // hdmiManager.setStandbyMode(true);

                    final HdmiPlaybackClient playbackClient = hdmiManager.getPlaybackClient();
                    resultOutput.append("HdmiPlaybackClient: ").append(playbackClient != null ? "Connected" : "Null").append("\n");

                    // not working, returns null
                    HdmiDeviceInfo deviceInfo = playbackClient.getActiveSource();
                    resultOutput.append("HdmiDeviceInfo: ").append(deviceInfo != null ? deviceInfo.getDisplayName() : "Null").append("\n");

                    // not working, callback is not called
                    playbackClient.queryDisplayStatus(new HdmiPlaybackClient.DisplayStatusCallback() {
                        @Override
                        public void onComplete(int status) {
                            resultOutput.append("Display status: ").append(status);
                            textResult.setText(resultOutput.toString());
                        }
                    });

                    // not working, no events coming
                    resultOutput.append("Start listening to events\n");
                    playbackClient.setVendorCommandListener(new HdmiControlManager.VendorCommandListener() {
                        @Override
                        public void onReceived(int srcAddress, int destAddress, byte[] params, boolean hasVendorId) {
                            resultOutput.append("Received event:\n");
                            resultOutput.append("--- address: ").append(srcAddress).append("\n");
                            resultOutput.append("--- destination: ").append(destAddress).append("\n");
                            resultOutput.append("--- params: ").append(params).append("\n");
                            textResult.setText(resultOutput.toString());
                        }

                        @Override
                        public void onControlStateChanged(boolean enabled, int reason) {
                            resultOutput.append("Device state changed:\n");
                            resultOutput.append("--- enabled: ").append(enabled).append("\n");
                            resultOutput.append("--- reason: ").append(reason).append("\n");
                            textResult.setText(resultOutput.toString());
                        }
                    });

                    pwrButton.setVisibility(View.VISIBLE);
                    pwrButton.setClickable(true);
                    pwrButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            playbackClient.sendStandby();
                        }
                    });
                } catch(Exception e) {
                    resultOutput.append("FAIL!\n");
                    resultOutput.append(e.getMessage());
                }

                textResult.setText(resultOutput.toString());
            }
        });
    }
}
