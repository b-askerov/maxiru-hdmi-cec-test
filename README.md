## HDMI CEC Test Application

### Prerequisites:

- Java Development Kit 8 (JDK8) (with JAVA_HOME set)

- Android Build Tools

- Android SDK (28)

- Copy and replace `./libs/android.jar` to `$ANDROID_SDK_DIR/platforms/android-28/android.jar`


### Building

Run `gradle clean && gradle assembleRelease`

The output apk will be at `./build/outputs/apk/release`

APK will already be signed with the platform certificate.

